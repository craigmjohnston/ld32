﻿using UnityEngine;
using System.Collections;

public enum RoomType {
    None, Stairs, DiningHall, Kitchen, Bedroom, Entrance
}

[RequireComponent(typeof(BoxCollider2D))]
public class Room : MonoBehaviour {
    public RoomType roomType;
    public int floor;
    public Rect availableClueSpace;

    public float Width {
        get { return boxCollider.size.x; }
    }

    public Vector2 RandomPointOnFloor {
        get { return new Vector2(
            transform.position.x + Random.Range(0, Width) - Width/2, 
            transform.position.y - gameManager.floorSpacing/2);}
    }

    protected tk2dSprite sprite;
    protected BoxCollider2D boxCollider;
    protected GameManager gameManager;

	// Use this for initialization
	void Start () {
	    gameManager = FindObjectOfType<GameManager>();
	    sprite = GetComponent<tk2dSprite>();
	    boxCollider = GetComponent<BoxCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}