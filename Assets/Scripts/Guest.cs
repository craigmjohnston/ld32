﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public enum GuestGender {
    Male, Female, Other
}

public enum GuestState {
    Idle, MovingToStairs, MovingToRoom, Talking, Dead, Arrested
}

public enum ClothingColour {
    Red, White, Violet, Blue, Burgundy, Puce, Black, Navy
}

public enum ClothingCondition {
    Fancy, Frayed
}

public enum ClothingMaterial {
    Linen, Silk
}

public enum SuspicionLevel {
    None, Innocent, Suspicious
}

public class Guest : MonoBehaviour {
    public float walkSpeed;
    public float wanderSpeed;
    public float floorSpacing;

    public GuestGender gender;

    public string firstName;
    public string lastName;

    public float minTimeInRoom;
    public float maxTimeInRoom;

    public bool hasClue;
    public InformationClue informationClue;
    public bool hasLore = false;
    public bool isMurderer;

    public string FullName {
        get { return firstName + " " + lastName; }
    }

    public ClothingColour clothingColour;
    public ClothingCondition clothingCondition;
    public ClothingMaterial clothingMaterial;

    public Room currentRoom;
    public Room targetRoom;

    public SuspicionLevel suspicion = SuspicionLevel.None;

    public List<string> discoveredInformation = new List<string>();
    public int conversationCount = 0;

    public GuestState state { get; protected set; }

    protected int CurrentFloor {
        get { return currentRoom.floor; }
    }

    public string[] maleHair;
    public string[] femaleHair;
    public Color[] eyeColours;
    public Color[] skinColours;
    public string[] maleClothes;
    public string[] femaleClothes;

    public tk2dSprite faceSprite;
    public tk2dSprite eyesSprite;
    public tk2dSpriteAnimator bodySprite;
    public tk2dSpriteAnimator hairSprite;

    protected tk2dSprite[] sprites;

    protected Room stairsTarget;

    protected GameManager gameManager;

    protected float roomTimer;
    protected Vector2 targetPosition;

    protected Vector2 lastPosition;

    protected tk2dSprite censor;

	// Use this for initialization
	void Awake () {
	    gameManager = FindObjectOfType<GameManager>();
        state = GuestState.Idle;
	    sprites = GetComponentsInChildren<tk2dSprite>();
	}

    void Start() {
        GenerateAppearance();
        censor = transform.FindChild("Censor").GetComponent<tk2dSprite>();
        censor.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
	    if (gameManager.state != GameState.Playing) return;
	    if (state == GuestState.Dead || state == GuestState.Arrested) return;
	    switch (state) {
	        case GuestState.Idle:
	            UpdateIdle();
	            break;
	        case GuestState.MovingToStairs:
	            UpdateMovingToStairs();
	            break;
	        case GuestState.MovingToRoom:
                UpdateMovingToRoom();
	            break;
            case GuestState.Talking:
	            break;
	        default:
	            throw new ArgumentOutOfRangeException();
	    }
	    foreach (tk2dSprite sprite in sprites) {
	        sprite.FlipX = transform.position.x > lastPosition.x;
	    }
	    lastPosition = transform.position;
	}

    public void GenerateAppearance() {
        // todo
        faceSprite.color = skinColours[UnityEngine.Random.Range(0, skinColours.Length)];
        eyesSprite.color = eyeColours[UnityEngine.Random.Range(0, eyeColours.Length)];
        if (gender == GuestGender.Male || gender == GuestGender.Other && UnityEngine.Random.value >= 0.5f) {
            hairSprite.Play(maleHair[UnityEngine.Random.Range(0, maleHair.Length)]);
            bodySprite.Play(maleClothes[UnityEngine.Random.Range(0, maleClothes.Length)]);
        } else { 
            hairSprite.Play(femaleHair[UnityEngine.Random.Range(0, femaleHair.Length)]);
            bodySprite.Play(femaleClothes[UnityEngine.Random.Range(0, femaleClothes.Length)]);
        }
        //transform.FindChild("Body").GetComponent<tk2dSpriteAnimator>().color;
    }

    protected void UpdateIdle() {
        if (targetRoom != null && currentRoom != targetRoom) {
            targetPosition = new Vector3(targetRoom.RandomPointOnFloor.x, transform.position.y) + Vector3.forward*transform.position.z;
            state = CurrentFloor != targetRoom.floor ? GuestState.MovingToStairs : GuestState.MovingToRoom;
            return;
        }
        if ((Vector2) transform.position != targetPosition) {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, wanderSpeed*Time.deltaTime);
        } else {
            targetPosition = new Vector3(currentRoom.RandomPointOnFloor.x, transform.position.y) + Vector3.forward*transform.position.z;
        }
        roomTimer -= Time.deltaTime;
	    if (roomTimer <= 0) {
	        GoToRandomRoom();
	    }
    }

    protected void UpdateMovingToStairs() {
        if (stairsTarget == null) {
	        stairsTarget = FindObjectsOfType<Room>().First(r => r.roomType == RoomType.Stairs && r.floor == CurrentFloor);
	    }
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(stairsTarget.transform.position.x, transform.position.y) + Vector3.forward*transform.position.z, walkSpeed * Time.deltaTime);
	    if (currentRoom == stairsTarget) {
	        transform.position += (targetRoom.floor > CurrentFloor ? Vector3.up : Vector3.down)*floorSpacing;
	        stairsTarget = null;
            state = GuestState.Idle;
	    }
    }

    protected void UpdateMovingToRoom() {
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, walkSpeed * Time.deltaTime);
        if (currentRoom == targetRoom) {
            roomTimer = UnityEngine.Random.Range(minTimeInRoom, maxTimeInRoom);
	        targetRoom = null;
            state = GuestState.Idle;
	    }
    }

    public void GoToRandomRoom() {
        do {
            var validRooms = gameManager.rooms.Where(r => r.roomType != RoomType.Stairs && r != currentRoom).ToList();
            targetRoom = validRooms[UnityEngine.Random.Range(0, validRooms.Count())];
        } while (targetRoom == currentRoom);
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Room") {
            currentRoom = other.GetComponent<Room>();
        }
    }

    public void Arrest() {
        state = GuestState.Arrested;
    }

    public void BeginTalking() {
        state = GuestState.Talking;
    }

    public void EndTalking() {
        state = GuestState.Idle;
    }

    public void Die() {
        state = GuestState.Dead;
        censor.gameObject.SetActive(true);
    }
}