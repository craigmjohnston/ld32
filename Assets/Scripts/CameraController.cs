﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
    public Transform target;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
	    transform.position = new Vector3(target.position.x, target.position.y + 0.24f, transform.position.z);
	}
}
