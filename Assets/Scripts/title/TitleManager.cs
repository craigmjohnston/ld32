﻿using UnityEngine;
using System.Collections;

public class TitleManager : MonoBehaviour {
    public string gameLevel;

    public RectTransform titlePanel;
    public RectTransform firstPanel;
    public RectTransform secondPanel;

	void Update () {
	    if (Input.anyKeyDown) {
	        if (titlePanel.gameObject.activeSelf) {
	            titlePanel.gameObject.SetActive(false);
                firstPanel.gameObject.SetActive(true);
            } else if (firstPanel.gameObject.activeSelf) {
                firstPanel.gameObject.SetActive(false);
                secondPanel.gameObject.SetActive(true);
            } else {
	            Application.LoadLevel(gameLevel);
            }
	    }
	}
}
