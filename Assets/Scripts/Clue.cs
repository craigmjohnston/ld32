﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ClueType {
    ClothingScrap, Item
}

[Serializable]
public class InformationClue {
    public ClueType clueType;
    public Dictionary<string, string> attributes = new Dictionary<string, string>();

    public override string ToString() {
        switch (clueType) {
            case ClueType.ClothingScrap:
                return "A scrap of " 
                    + attributes["clothingcolour"].ToLower()
                    + " " 
                    + attributes["clothingcondition"].ToLower()
                    + " " 
                    + attributes["clothingmaterial"].ToLower()
                    + ".";
            case ClueType.Item:
            default: return base.ToString();
        }
    }
}

public class Clue : MonoBehaviour {
    public ClueType clueType;
    // todo public string label;
    // todo public string description;
    public Dictionary<string, string> attributes = new Dictionary<string, string>();

	void Start () {
	    transform.position -= Vector3.up*0.2f;
	}
	
	void Update () {
	
	}

    public InformationClue ToInformationClue() {
        return new InformationClue {
            clueType = clueType,
            attributes = attributes
        };
    }
}