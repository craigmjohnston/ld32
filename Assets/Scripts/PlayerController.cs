﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Boomlagoon.JSON;
using UnityEngine.UI;

public enum PlayerState {
    Idle, Talking
}

public class PlayerController : MonoBehaviour {
    public float moveSpeed;
    public float floorSpacing;

    public RectTransform talkingPanel;
    public Text talkingBodyText;
    public Text talkingAuthorText;

    public TextAsset conversationData;

    public Vector2 arrestPosition;

    public Room currentRoom;
    protected BoxCollider2D boxCollider;

    protected List<Clue> collidingClues = new List<Clue>();
    protected List<Guest> collidingGuests = new List<Guest>();

    protected PlayerState state = PlayerState.Idle;
    protected Guest talkingToGuest;

    protected GameManager gameManager;

    protected bool leftLast = false;
    protected tk2dSprite sprite;

    public RoomType CurrentRoomType {
        get { return currentRoom.roomType; }
    }

	void Start () {
	    boxCollider = GetComponent<BoxCollider2D>();
	    gameManager = FindObjectOfType<GameManager>();
	    sprite = GetComponent<tk2dSprite>();
	}
	
	void Update () {
	    switch (state) {
	        case PlayerState.Idle:
                IdleUpdate();
	            break;
	        case PlayerState.Talking:
                TalkingUpdate();
	            break;
	        default:
	            throw new ArgumentOutOfRangeException();
	    }
        // todo weeee magic numbers
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -1.55f, 2.5f), transform.position.y, transform.position.z);
	}

    protected void IdleUpdate() {
        if (Input.GetAxisRaw("Horizontal") != 0) {
	        transform.position += Vector3.right * Input.GetAxisRaw("Horizontal")*moveSpeed*Time.deltaTime;
            leftLast = Input.GetAxisRaw("Horizontal") < 0;
        } else if (Input.GetButtonDown("UpStairs") && CurrentRoomType == RoomType.Stairs) {
            if (currentRoom.floor != 2) {
                transform.position += Vector3.up*floorSpacing;
            }
        } else if (Input.GetButtonDown("DownStairs") && CurrentRoomType == RoomType.Stairs) {
            if (currentRoom.floor != 0) {
                transform.position += Vector3.down*floorSpacing;
            }
        }
	    if (Input.GetButtonDown("PickUp") && collidingClues.Any()) {
	        Clue clue = collidingClues.OrderBy(c => Vector2.Distance(transform.position, c.transform.position)).First();
            PickUpClue(clue);
        } else if (Input.GetButtonDown("Talk") && collidingGuests.Any()) {
            Guest guestToTalk = FirstInteractableGuest();
            if (guestToTalk != null) {
                TalkToGuest(guestToTalk);
            }
        } else if (Input.GetButtonDown("Arrest") && collidingGuests.Any()) {
            Guest guestToArrest = FirstInteractableGuest();
            if (guestToArrest != null) {
                ArrestGuest(guestToArrest);
            }
        }
        sprite.FlipX = !leftLast;
    }

    protected Guest FirstInteractableGuest() {
        return collidingGuests
            .Where(g => (leftLast && g.transform.position.x < transform.position.x) || (!leftLast && g.transform.position.x > transform.position.x))
            .OrderBy(g => Mathf.Abs(transform.position.x - g.transform.position.x))
            .FirstOrDefault(g => g.state != GuestState.Dead && g.state != GuestState.Arrested);
    }

    protected void PickUpClue(Clue clue) {
        gameManager.AddClue(clue.ToInformationClue());
	    collidingClues.Remove(clue);
        Destroy(clue.gameObject);
    }

    protected void TalkToGuest(Guest guest) {
        guest.conversationCount += 1;
        if (guest.conversationCount == 1) {
            guest.discoveredInformation.Add("Wears clothing made from " + guest.clothingCondition.ToString().ToLower() + ", " + guest.clothingColour.ToString().ToLower() + " " + guest.clothingMaterial.ToString().ToLower() + ".");
            gameManager.Flash("You made a note of " + guest.FullName + "'" + (guest.FullName.Last() == 's' ? "" : "s") + " appearance in your notebook.");
        }
        talkingToGuest = guest;
        guest.BeginTalking();
        state = PlayerState.Talking;
        if (!guest.hasClue) {
            talkingBodyText.text = GetRandomConversationText(guest.hasLore ? "lore" : "filler");
        }
        if (guest.hasLore) {
            gameManager.AddLore(talkingBodyText.text);
            guest.hasLore = false;
        }
        talkingAuthorText.text = "- " + guest.FullName + ", " + gameManager.TimeString12H + ".";
        talkingPanel.gameObject.SetActive(true);
    }

    protected void ArrestGuest(Guest guest) {
        if (gameManager.currentArrest) {
            gameManager.Flash("You've already arrested someone. Having doubts already?");
            return;
        }
        gameManager.currentArrest = true;
        guest.Arrest();
        Vector2 position = arrestPosition;
        FadeToPosition(position);
        guest.transform.position = (Vector3)position + Vector3.forward*transform.position.z;
        gameManager.Flash("You have arrested " + guest.FullName + ". Only time will tell if this was the right decision.");
    }

    public void FadeToPosition(Vector2 position) {
        // todo fade
        transform.position = (Vector3)position + Vector3.forward*transform.position.z;
    }

    protected void TalkingUpdate() {
        if (Input.GetButtonDown("Talk")) {
            state = PlayerState.Idle;
            talkingToGuest.EndTalking();
            talkingToGuest = null;
            talkingPanel.gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Room") {
            currentRoom = other.GetComponent<Room>();
        } else if (other.tag == "Clue") {
            collidingClues.Add(other.GetComponent<Clue>());
        } else if (other.tag == "GuestTalkCollider") {
            collidingGuests.Add(other.transform.parent.GetComponent<Guest>());
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "Clue") {
            collidingClues.Remove(other.GetComponent<Clue>());
        } else if (other.tag == "GuestTalkCollider") {
            collidingGuests.Remove(other.transform.parent.GetComponent<Guest>());
        }
    }

    protected string GetRandomConversationText(string group) {
        JSONArray groupArray = JSONObject.Parse(conversationData.text).GetArray(group);
        return groupArray[UnityEngine.Random.Range(0, groupArray.Length)].Str;
    }
}