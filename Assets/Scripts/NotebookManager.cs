﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

public class NotebookManager : MonoBehaviour {
    public Vector2 hiddenPosition;

    public RectTransform mainCharacterPage;
    public RectTransform lorePage;
    public RectTransform cluesPage;
    public RectTransform characterInfoPage;

    public Button guestButtonPrefab;
    public Text textItemPrefab;

    public RectTransform mainCharacterLeftPage;
    public RectTransform mainCharacterRightPage;

    public RectTransform loreLeftPage;
    public RectTransform loreRightPage;

    public RectTransform cluesLeftPage;
    public RectTransform cluesRightPage;

    public RectTransform characterInfoLeftPage;
    public RectTransform characterInfoRightPage;

    public Text characterNameText;

    protected GameManager gameManager;
    protected bool hidden = true;
    protected Vector2 showingPosition;

    protected RectTransform rectTransform;

    protected Guest currentGuest;

	void Start () {
	    gameManager = FindObjectOfType<GameManager>();
	    rectTransform = GetComponent<RectTransform>();
	    showingPosition = rectTransform.position;
	    rectTransform.position = hiddenPosition;
	}
	
	void Update () {
	
	}

    public void Show() {
        ShowMainCharacterPage();
        rectTransform.position = showingPosition;
        hidden = false;
    }

    public void Hide() {
        rectTransform.position = hiddenPosition;
        hidden = true;
    }

    public void ShowMainCharacterPage() {
        DisableAllPageObjects();
        mainCharacterPage.gameObject.SetActive(true);
        foreach (Transform guestButton in mainCharacterLeftPage) {
            Destroy(guestButton.gameObject);
        }
        foreach (Transform guestButton in mainCharacterRightPage) {
            Destroy(guestButton.gameObject);
        }
        int index = 0;
        foreach (Guest guest in gameManager.originalGuests.Take(gameManager.originalGuests.Count / 2)) {
            var guestButton = DrawGuestButton(guest);
            int localIndex = index++;
            guestButton.onClick.AddListener(() => {
                OpenCharacterPage(localIndex);
            });
            guestButton.transform.parent = mainCharacterLeftPage;
        }
        // todo this is lazy
        foreach (Guest guest in gameManager.originalGuests.Skip(gameManager.originalGuests.Count / 2)) {
            var guestButton = DrawGuestButton(guest);
            int localIndex = index++;
            guestButton.onClick.AddListener(() => {
                OpenCharacterPage(localIndex);
            });
            guestButton.transform.parent = mainCharacterRightPage;
        }
    }

    protected Button DrawGuestButton(Guest guest) {
        Button guestButton = Instantiate(guestButtonPrefab);
        guestButton.GetComponentInChildren<Text>().text = guest.FullName;
        if (guest.state == GuestState.Dead) {
            guestButton.transform.FindChild("Cross").GetComponent<Image>().enabled = true;
        }
        return guestButton;
    }

    public void ShowLorePage() {
        DisableAllPageObjects();
        lorePage.gameObject.SetActive(true);
        foreach (Transform textItem in loreLeftPage) {
            Destroy(textItem.gameObject);
        }
        foreach (Transform textItem in loreRightPage) {
            Destroy(textItem.gameObject);
        }
        foreach (string lore in gameManager.loreCollected) {
            var textItem = Instantiate(textItemPrefab);
            textItem.transform.parent = loreLeftPage;
            textItem.text = lore;
        }
    }

    public void ShowCluesPage() {
        DisableAllPageObjects();
        cluesPage.gameObject.SetActive(true);
        foreach (Transform textItem in cluesLeftPage) {
            Destroy(textItem.gameObject);
        }
        foreach (Transform textItem in cluesRightPage) {
            Destroy(textItem.gameObject);
        }
        foreach (InformationClue clue in gameManager.cluesCollected) {
            var textItem = Instantiate(textItemPrefab);
            textItem.transform.parent = cluesLeftPage;
            textItem.text = clue.ToString();
        }
    }

    public void ShowCharacterInfoPage() {
        DisableAllPageObjects();
        characterInfoPage.gameObject.SetActive(true);
    }

    protected void DisableAllPageObjects() {
        mainCharacterPage.gameObject.SetActive(false);
        lorePage.gameObject.SetActive(false);
        cluesPage.gameObject.SetActive(false);
        characterInfoPage.gameObject.SetActive(false);
    }

    protected void OpenCharacterPage(int index) {
        ShowCharacterInfoPage();
        currentGuest = gameManager.originalGuests[index];
        characterNameText.text = currentGuest.FullName;
        foreach (Transform textItem in characterInfoLeftPage) {
            Destroy(textItem.gameObject);
        }
        foreach (string information in currentGuest.discoveredInformation) {
            var textItem = Instantiate(textItemPrefab);
            textItem.transform.parent = characterInfoLeftPage;
            textItem.text = information;
        }
    }

    public void SetInnocent() {
        currentGuest.suspicion = SuspicionLevel.Innocent;
    }

    public void SetSuspicious() {
        currentGuest.suspicion = SuspicionLevel.Suspicious;
    }
}