﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public enum GameState {
    Intro, Playing, Voting, Notebook
}

public struct ClothingColourActual {
    public ClothingColour colourEnum;
    public Color actualColour;
}

public class GameManager : MonoBehaviour {
    public Rect spawnRect;
    public float spawnZ;

    public float floorSpacing;

    public int startingHour;
    public float timeMultiplier;
    public Text clockText;

    public int minCluesInMurderRoom;
    public int maxCluesInMurderRoom;

    public int minCluesInSurroundingRooms;
    public int maxCluesInSurroundingRooms;

    public float minMurderCooldown;
    public float maxMurderCooldown;

    public int numberOfGuests;
    public TextAsset surnames;
    public TextAsset maleNames;
    public TextAsset femaleNames;

    public ClothingColourActual[] actualClothingColours;

    public Clue cluePrefab;
    public Guest guestPrefab;

    [HideInInspector] public List<Guest> originalGuests = new List<Guest>();
    [HideInInspector] public List<Guest> remainingGuests = new List<Guest>();
    protected Guest murderer;

    public RectTransform guestButtonsPanel;
    public Button guestButtonPrefab;
    public RectTransform votingPanel;

    [HideInInspector] public Room[] rooms;

    public GameState state { get; protected set; }
    protected GameState oldState;

    [HideInInspector] public List<string> loreCollected = new List<string>(); 
    [HideInInspector] public List<InformationClue> cluesCollected = new List<InformationClue>();

    public RectTransform losePanel;
    public RectTransform winPanel;

    public int maxBadArrests;

    public Text flashText;

    protected float murderCooldownTimer;
    protected bool murderLastHour = false;
    protected bool murder;

    protected int hour;
    protected int minute;

    protected string[] maleNameList;
    protected string[] femaleNameList;
    protected string[] surnameList;

    protected PlayerController player;
    protected NotebookManager notebookManager;

    [HideInInspector] public bool currentArrest = false;
    protected int badArrests = 0;

    protected float flashStarted;

    public string TimeString {
        get { return hour.ToString("D2") + ":" + minute.ToString("D2"); }
    }

    public string TimeString12H {
        get {
            return (hour > 12 ? hour - 12 : hour) + ":" + minute.ToString("D2") + (hour > 12 ? "pm" : "am");
        }
    }

    protected float minuteTimer;

	void Start () {
        state = GameState.Playing; // todo should start on intro
	    player = FindObjectOfType<PlayerController>();
	    notebookManager = FindObjectOfType<NotebookManager>();
	    hour = startingHour;
	    clockText.text = TimeString;
        // todo this isn't how it's going to work
	    maleNameList = maleNames.text.Split('\n');
        femaleNameList = femaleNames.text.Split('\n');
        surnameList = surnames.text.Split('\n');
        GenerateGuests();
	    originalGuests = FindObjectsOfType<Guest>().ToList();
	    remainingGuests = originalGuests.ToList();
        murderer = originalGuests[Random.Range(0, originalGuests.Count)];
        murderer.isMurderer = true;
        // rooms
	    rooms = FindObjectsOfType<Room>();
	    foreach (Guest guest in originalGuests) {
	        guest.GoToRandomRoom();
	    }
	}
	
	void Update () {
	    if (flashText.gameObject.activeSelf && Time.realtimeSinceStartup - flashStarted > 5f) {
	        flashText.gameObject.SetActive(false);
	    }
	    if (remainingGuests.Count == 0) {
	        LoseGame();
	    }
	    switch (state) {
	        case GameState.Intro:
                UpdateIntro();
	            break;
	        case GameState.Playing:
                UpdatePlaying();
	            break;
	        case GameState.Voting:
                UpdateVoting();
	            break;
            case GameState.Notebook:
	            break;
	        default:
	            throw new ArgumentOutOfRangeException();
	    }
	    if (state == GameState.Playing || state == GameState.Voting) {
	        if (Input.GetButtonDown("Notebook")) {
	            notebookManager.Show();
	            oldState = state;
	            state = GameState.Notebook;
	        }
        } else if (state == GameState.Notebook) {
            if (Input.GetButtonDown("Notebook")) {
	            notebookManager.Hide();
	            state = oldState;
	        }
        }
	}

    protected void WinGame() {
        winPanel.gameObject.SetActive(true);
    }

    protected void LoseGame() {
        losePanel.gameObject.SetActive(true);
    }

    public void Flash(string text) {
        flashText.text = text;
        flashText.gameObject.SetActive(true);
        flashStarted = Time.realtimeSinceStartup;
    }

    protected void UpdatePlaying() {
        if (murder) {
	        murderCooldownTimer -= Time.deltaTime*timeMultiplier;
	        if (murderCooldownTimer <= 0 && murderer.currentRoom != player.currentRoom) {
                // choose a victim, prefer arrested guests
	            Guest victim = remainingGuests.OrderBy(elem => Guid.NewGuid()).FirstOrDefault(g => g.currentRoom != player.currentRoom && g != murderer && g.state != GuestState.Dead && g.currentRoom.roomType != RoomType.Stairs);
	            Guest arrested = originalGuests.FirstOrDefault(g => g.currentRoom != player.currentRoom && g != murderer && g.state == GuestState.Arrested && g.state != GuestState.Dead);
	            if (arrested != null) {
	                victim = arrested;
	            }
	            if (victim != null) {
	                if (victim == arrested) {
	                    currentArrest = false;
	                }
	                MurderGuest(victim);
	                murder = false;
	                murderLastHour = true;
	                if (currentArrest) {
	                    badArrests += 1;
	                    if (badArrests >= maxBadArrests) {
	                        LoseGame();
	                    }
	                }
	            }
	        }
	    }
	    ProgressClock();
    }

    protected void UpdateVoting() {
        
    }

    protected void UpdateIntro() {
        
    }

    protected void ProgressClock() {
        minuteTimer += Time.deltaTime * timeMultiplier;
	    if (minuteTimer >= 60) {
	        minuteTimer -= 60;
	        minute += 1;
	        if (minute >= 60) {
	            minute = 0;
	            hour += 1;
	            if (murderer.state == GuestState.Arrested) {
	                WinGame();
	            }
	            if (hour >= 24) {
	                hour = 0;
	            }
	            if (!murderLastHour || Random.value >= 0.5f) {
	                murder = true;
	                murderCooldownTimer = Random.Range(minMurderCooldown, maxMurderCooldown);
	            } else {
	                murder = false;
	                murderLastHour = false;
	            }
                //HoldVote();
	        }
	        clockText.text = TimeString;
	    }
    }

    protected void HoldVote() {
        state = GameState.Voting;
        var entranceHall = rooms.First(r => r.roomType == RoomType.Entrance);
        foreach (Guest guest in remainingGuests) {
            guest.transform.position = (Vector3)entranceHall.RandomPointOnFloor + Vector3.up*floorSpacing/2f + Vector3.forward*guest.transform.position.z;
        }
        votingPanel.gameObject.SetActive(true);
        // clear the existing buttons
        foreach (Transform guestButton in guestButtonsPanel) {
            Destroy(guestButton.gameObject);
        }
        // make new buttons for the remaining guests
        int index = 0;
        foreach (Guest guest in remainingGuests) {
            var guestButton = Instantiate(guestButtonPrefab);
            guestButton.transform.parent = guestButtonsPanel;
            guestButton.GetComponentInChildren<Text>().text = guest.FullName;
            int localIndex = index++;
            guestButton.onClick.AddListener(() => { VoteForGuest(localIndex); });
        }
    }

    protected void MurderGuest(Guest guest) {
        string floor = "this floor";
        if (guest.currentRoom.floor > player.currentRoom.floor) {
            floor = "a floor above";
        } else if (guest.currentRoom.floor < player.currentRoom.floor) {
            floor = "a floor below";
        }
        Flash("A scream! Sounds like another murder is happening on " + floor + "!");
        guest.Die();
        CreateCluesFromMurder(guest, murderer, guest.currentRoom);
        remainingGuests.Remove(guest);
        foreach (Guest g in remainingGuests) {
            g.targetRoom = guest.currentRoom;
        }
    }

    public void GenerateGuests() {
        for (int i = 0; i < numberOfGuests; i++) {
            var guest = GenerateGuest();
        }
    }

    public void AddLore(string lore) {
        loreCollected.Add(lore);
    }

    public void AddClue(InformationClue clue) {
        cluesCollected.Add(clue);
        Flash("Collected " + clue.ToString().ToLower() + " Added to notebook.");
    }

    public void VoteForGuest(int index) {
        Guest guest = remainingGuests[index];
        Debug.Log("Voted for: " + guest.FullName);
        if (guest.isMurderer) {
            winPanel.gameObject.SetActive(true);
        } else {
            Debug.Log("Wasn't the murderer.");
        }
        Destroy(guest.gameObject);
        state = GameState.Playing;
        votingPanel.gameObject.SetActive(false);
    }

    public void AbstainFromVoting() {
        Debug.Log("Abstained.");
        state = GameState.Playing;
        votingPanel.gameObject.SetActive(false);
    }

    protected Guest GenerateGuest() {
        var guest = Instantiate(guestPrefab);
        guest.transform.position = (Vector3)RandomPoint(spawnRect) + Vector3.forward * spawnZ;
        guest.gender = (GuestGender)Random.Range(0, Enum.GetValues(typeof (GuestGender)).Length);
        if (guest.gender == GuestGender.Male || (guest.gender == GuestGender.Other && Random.value >= 0.5f)) {
            guest.firstName = maleNameList[Random.Range(0, maleNameList.Length)];
        } else {
            guest.firstName = femaleNameList[Random.Range(0, femaleNameList.Length)];
        }
        guest.lastName = surnameList[Random.Range(0, surnameList.Length)];
        guest.clothingColour = (ClothingColour)Random.Range(0, Enum.GetValues(typeof (ClothingColour)).Length);
        guest.clothingCondition = (ClothingCondition)Random.Range(0, Enum.GetValues(typeof (ClothingCondition)).Length);
        guest.clothingMaterial = (ClothingMaterial)Random.Range(0, Enum.GetValues(typeof (ClothingMaterial)).Length);
        guest.hasLore = Random.value >= 0.5f;
        return guest;
    }

    public void CreateCluesFromMurder(Guest victim, Guest murderer, Room room) {
        int numberOfMurderRoomClues = Random.Range(minCluesInMurderRoom, maxCluesInMurderRoom + 1);
        int numberOfSurroundingRoomClues = Random.Range(minCluesInSurroundingRooms, maxCluesInSurroundingRooms + 1);
        List<Clue> murderRoomClues = new List<Clue>();
        List<Clue> surroundingRoomClues = new List<Clue>();
        for (int i = 0; i < numberOfMurderRoomClues; i++) {
            murderRoomClues.Add(GenerateClue(victim, murderer));
        }
        for (int i = 0; i < numberOfSurroundingRoomClues; i++) {
            surroundingRoomClues.Add(GenerateClue(victim, murderer));
        }
        // place the clues
        PlaceCluesInRoom(room, murderRoomClues.ToArray());
        // todo place surrounding clues
    }

    protected void PlaceCluesInRoom(Room room, params Clue[] clues) {
        foreach (var clue in clues) {
            clue.transform.position = room.transform.position + (Vector3) RandomPoint(room.availableClueSpace);
        }
    }

    protected Clue GenerateClue(Guest victim, Guest murderer) {
        var clue = Instantiate(cluePrefab);
        // todo attributes
        Guest guest = Random.value > 0.5f ? victim : murderer;
        int randomValue = Random.Range(0, 3) + 1;
        clue.clueType = ClueType.ClothingScrap;;//Random.value > 0.5f ? ClueType.ClothingScrap : ClueType.Item;
        if (clue.clueType == ClueType.ClothingScrap) {
            clue.attributes.Add("clothingcolour", guest.clothingColour.ToString());
            clue.attributes.Add("clothingcondition", guest.clothingCondition.ToString());
            clue.attributes.Add("clothingmaterial", guest.clothingMaterial.ToString());
        } else if (clue.clueType == ClueType.Item) {
            // todo item
        }
        return clue;
    }

    public static Vector2 RandomPoint(Rect rect) {
        return new Vector2(rect.xMin + Random.Range(0, rect.width), rect.yMin + Random.Range(0, rect.height));
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(spawnRect.center, new Vector3(spawnRect.width, spawnRect.height));
    }
}